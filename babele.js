Hooks.on('init', () => {
    if (typeof Babele !== 'undefined') {
      Babele.get().register({
        module: 'pf2-de',
        lang: 'de',
        dir: 'babele/de'
      })
    }
  })
  